# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['User']


class User:
    __metaclass__ = PoolMeta
    __name__ = "res.user"
    healthprof = fields.Many2One('gnuhealth.healthprofessional',
        'Health Professional')

    # @classmethod
    # def __setup__(cls):
    #     super(User, cls).__setup__()
